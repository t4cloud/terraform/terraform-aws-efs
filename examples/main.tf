module "efs-NAME" {
  source    = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-efs.git?ref=master"
  name      = "NAME"
  vpc_id    = "vpc-xxxxx"
  encrypted = false

  access_sg_ids = [
    "sg-xxxx",
  ]

  subnets = [
    "subnet-xxxxxxxx",
    "subnet-xxxxxxxx",
    "subnet-xxxxxxxx",
  ]
}
